const history_model = require('../models').History;
const sequelize = require('sequelize');

// Get all histories for which a given user was a giver
module.exports.getGiverHistory = async(username, callback) => {
    if(!username)
        return null;
        
    history_model.findAll({
        where: {
            giver: username
        },
        attributes : ['giver', 'recipient', 'message']
    }).then(histories => {
        callback(histories);
    });

    return history;
}

// Get all histories for which a given user was a recipient
module.exports.getGiverHistory = async(username, callback) => {
    if(!username)
        return null;
        
    history_model.findAll({
        where: {
            recipient: username
        },
        attributes: ['giver', 'recipient', 'message']
    }).then(histories => {
        callback(histories);
    });

    return history;
}

// Returns a single history based on its ID
module.exports.getHistory = async(id) => {
    if(!id)
        return null;

    let history = history_model.findOne({
        where: {
            id: id
        }
    });

    return history;
}

// Get all histories in database
module.exports.getAllHistories = async(callback) => {
    history_model.findAll({
        attributes: ['recipient', 'giver']
    }).then(histories=>{
        callback(histories);
    });
}