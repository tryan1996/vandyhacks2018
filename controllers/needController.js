const need_model = require('../models').Need;

// Returns a single need from a given ID
module.exports.getNeed = async (id) => {
    if (!id)
        return null;

    let need = need_model.findOne({
        where: {
            id: id
        }
    });

    return need;
}

// Returns all needs from a giver by their username
module.exports.getNeedsByGiver = async (username) => {
    if (!username)
        return null;

    let needs = need_model.findAll({
        where: {
            giver: username
        }
    });

    return needs;
}

// Returns all needs from a recipient by their username
module.exports.getNeedsByRecipient = async (username, callback) => {
    if (!username)
        return null;

    need_model.findAll({
        where: {
            recipient: username
        }
    }).then(needs => {
        callback(needs);
    });
}

// Returns all needs with a waiting status
module.exports.getAllNeeds = async () => {
    need_model.findAll({
        where: {
            status: 'WAITING'
        }
    }).then(needs => {
        callback(needs);
    });
}

// Inserts a single new need into database
module.exports.insertNeed = async (recipient, giver, status, description, latlon) => {
    var newNeed = {
        recipient: recipient,
        giver: giver,
        status: status,
        description: description,
        latlon: latlon
    }
    need_model.create(newNeed);
}

// Updates an existing need by ID to add a giver
module.exports.updateGiver = async (id, giver) => {
    need_model.update({
        giver: giver,
        status: 'PENDING'
    }, {
        where: {
            id: id
        }
    });
}