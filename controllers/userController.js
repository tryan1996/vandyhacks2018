// User data model
const user_model = require('../models').User;

// For hashing and salting
const bcrypt = require('bcrypt');
const saltRounds = 10;

// Returns a single user based on their ID
module.exports.getUser = async(id) => {
    if(!id)
        return null;

    let user = user_model.findOne({
        where: {
            id: id
        }
    });

    return user;
}

// Returns a single user based on their username
module.exports.getUserByUsername = async(username) => {
    if(!username)
        return null;

    let user = user_model.findOne({
        where: {
            username: username
        }
    });

    return user;
}

// Returns a single user based on their email
module.exports.getUserByUsername = async(email) => {
    if(!email)
        return null;

    let user = user_model.findOne({
        where: {
            email: email
        }
    });

    return user;
}

module.exports.insertUser = async(username, email, password, settings) => {
    bcrypt.hash(password, saltRounds, function(err, hash) {
        if(err){
            console.log(err);
            return;
        }
        var newUser = {
            username: username,
            email: email,
            password: hash,
            settings: settings
        }
        user_model.create(newUser);
    });
}

module.exports.getAllUsers = async(callback) => {
    user_model.findAll({
        attributes : ['username']
    }).then(users =>{
        callback(users);
    });
}

// Return user object if successful authentication, empty object if not
module.exports.authenticate = async(username, password, callback) => {
    user_model.findOne({
        where : {
            username: username
        }
    }).then(user => {
        if(!user)
            callback([]);
        else{
            bcrypt.compare(password, user.dataValues.password, function(err, res) {
                if(res)
                    callback(user);
                else
                    callback([]);
            });
        }
    }); 
}