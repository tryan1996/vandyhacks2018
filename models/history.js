'use strict';
module.exports = (sequelize, DataTypes) => {
  const History = sequelize.define('History', {
    recipient: DataTypes.STRING,
    giver: DataTypes.STRING,
    path: DataTypes.STRING,
    code: DataTypes.STRING,
    message: DataTypes.STRING
  }, {});
  History.associate = function(models) {
    // associations can be defined here

    History.hasOne(models.Need,{
      as: 'Need'
    });
  };
  return History;
};