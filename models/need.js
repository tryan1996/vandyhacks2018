'use strict';
module.exports = (sequelize, DataTypes) => {
  const Need = sequelize.define('Need', {
    recipient: {
      type:DataTypes.STRING,
      references:{
        model:sequelize.models.Users,
        key:'id'
      }
    },
    status: {
      type: DataTypes.ENUM,
      values: ['WAITING','PENDING','FULFILLED']
    },
    giver: {
      type:DataTypes.STRING,
      references:{
        model:sequelize.models.Users,
        key:'id'
      }
    },
    description: DataTypes.STRING,
    latlon: DataTypes.STRING
  }, {});
  Need.associate = function(models) {
    // associations can be defined here

    Need.belongsTo(models.User, 
      {
        foreignKey: 'username',
        targetKey: 'id',

      });
      Need.hasOne(models.History,
        {as: 'Histroy'});
  };
  return Need;
};