// Node Modules
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const session = require('express-session');
const fs = require('fs');
const cors = require('cors');
const request = require('request');
// Sequelize Controllers
const historyController = require('../controllers/historyController');
const userController = require('../controllers/userController');
const needController = require('../controllers/needController');
//Google Maps config
var NodeGeocoder = require('node-geocoder');
var options = {
    provider: 'google',
    apiKey: 'AIzaSyCJreAK-7TOdJzojPxFoynYU39U7bDgrcc',
};

var geocoder = NodeGeocoder(options);
// Express app object
var app = express();
// Enable body-parser for JSON-Encoded bodies
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
// Set up sessions
app.use(
    session({
        secret: 'dmaksndskandjk-WKUBUTTLAB-dasmdkas',
        key: 'dmaksndskandjk-WKUBUTTLAB-dasmdkas',
        resave: false,
        saveUninitialized: true,
        cookie: {
            httpOnly: false,
            expires: new Date(Date.now() + 60 * 60 * 1000)
        }
    })
);
//Set up CORS
app.use(cors());

// Registers a new user
app.post('/register', function (req, res) {
    console.log("register");
    if (req.body.username && req.body.email && req.body.password) {
        var settings = '/settings/' + req.body.username + '.json'; // TODO: create a settings file for each user
        userController.insertUser(req.body.username, req.body.email, req.body.password, settings);
        res.send('New user inserted successfully');
    } else {
        res.send('Unable to insert user: missing fields');
    }
});

// Logs out a user
app.post('/logout', function (req, res) {
    req.session.destroy();
    res.send('Log out successful');
});

// Returns user object if authentication successful
app.post('/login', function(req, res){
    if(req.body.username && req.body.password){
        userController.authenticate(req.body.username,req.body.password, function(result){
            req.session.email = result.dataValues.email;
            req.session.username = result.dataValues.username;
            res.send('Login successful.');
        });
    } else
        res.send('Error: No username and password provided.');
});

// Creates a single new need
app.post('/newneed', function (req, res) {
    if (!req.session.username)
        res.send('Log in necessary');
    else {
        if (req.body.username && req.body.description && req.body.latlon) {
            needController.insertNeed(req.body.username, null, 'WAITING', req.body.description, req.body.latlon);
            res.send('New need created successfully');
        } else {
            res.send('Error: Missing fields');
        }
    }
});

// returns the user's username if logged in
app.get('/sessioncheck', function (req, res) {
    if (req.session.username)
        res.send(req.session.username);
    res.send('Not logged in.');
});

// Returns all needs
app.get('/allneeds', function (req, res) {
    if (!req.session.username)
        res.send('Log in necessary');
    else {
        needController.getAllNeeds(function (result) {
            res.send(result);
        });
    }
});

// Returns all needs for a given recipient
app.get('/needsbyrecipient', function (req, res) {
    if (!req.session.username)
        res.send('Log in necessary');
    else if(!req.body.username)
        res.send('Please pass a username in the body.');
    else {
        needController.getNeedsByRecipient(req.body.username,function (result) {
            res.send(result);
        });
    }
});

// Returns all histories
app.get('/allhistories', function (req, res) {
    if (!req.session.username)
        res.send('Log in necessary');
    else {
        historyController.getAllHistories(function (result) {
            res.send(result);
        });
    }
});

// Get all history objects for a certain giver
app.get('/giverhistory', function (req, res) {
    if (!req.session.username)
        res.send('Log in necessary');
    else {
        if (req.body.username) {
            historyController.getGiverHistory(req.body.username, function (result) {
                res.send(result);
            });
        } else {
            res.send('Request failed: No username provided.');
        }
    }
});

// Get all history objects for a certain recipient
app.get('/recipienthistory', function (req, res) {
    if (!req.session.username)
        res.send('Log in necessary');
    else {
        if (req.body.username) {
            historyController.getRecipientHistory(req.body.username, function (result) {
                res.send(result);
            });
        } else {
            res.send('Request failed: No username provided.');
        }
    }
});

// Updates a need to have a giver, and changes its status to pending
app.post('/updategiver', function (req, res) {
    if (!req.session.username)
        res.send('Log in necessary');
    else {
        if (req.body.username && req.body.id) {
            needController.updateGiver(req.body.id, req.body.username);
            
        } else {
            res.send('Request failed: Missing fields');
        }
    }
});

app.post('/getLocations', function (req, res) {
    if(req.session.username){
        if (req.body.lat && req.body.lon) {
            var nearbyRequest = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?type=restaurant&location=' + req.body.lat + ',' + req.body.lon + '&radius=2000&key=' + options.apiKey;
            request(nearbyRequest, function (error, response, body) {
                if (error) {
                    res.send("Error requesting nearby places.");
                    console.log(error);
                } else {
                    var results = JSON.parse(body)["results"];
                    if(results.length>10)
                        results = results.slice(0,10);
                    res.send(results);
                }
            });
        } else {
            res.send("Please provide lat and lon.");
        }
    } else{
        res.send("Please log in.");
    }
});

// Start listening on port 8080
app.listen(8080);
console.log('Listening at port 8080...');
